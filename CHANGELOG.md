# Changelog

## [2.0.0] - 2019-10-31
### Changed
- Updates vendor name

## [1.2.0] - 2018-10-16
### Changed
- Updates 'needs update' response to return error json rather than exception

## [1.1.0] - 2018-08-20
### Changed
- Updated readme to reflect public status of the package on packagist

## [1.0.0] - 2018-08-16
### Added
- All initial files
