# API Force App Update

This package provides migrations and middleware for locking access to the API where the app version has been blacklisted.

### Pulling in with composer:

1. Install the package:
`composer require e3creative/api-force-app-update`


## Customisation

The error message given by the middleware is customisable in `config/api-force-app-update.php` if you publish the
package's config file:
`php artisan vendor:publish`


## How to Use
1. Add the middleware to the `$routeMiddleware` property of your `app/Http/Kernel.php`:
```
/**
 * The application's route middleware.
 *
 * These middleware may be assigned to groups or used individually.
 *
 * @var array
 */
protected $routeMiddleware = [
    ...,
    'force_app_update' => \E3Creative\ApiForceAppUpdate\Middleware\ForceUpdate::class,
];
```

2. Add the middleware to the routes you want to protect, or to the `api` middleware group in `Kernel.php` if you want it
to be applied univerally to the `api` routes.

```
Route::resource('users', 'UserController')->middleware('force_app_update');
```

4. Run the new migrations with `php artisan migrate`
3. Blacklist versions by adding them into your `blacklisted_versions` table
