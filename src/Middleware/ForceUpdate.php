<?php

namespace E3Creative\ApiForceAppUpdate\Middleware;

use Closure;
use App\Services\Response;
use Illuminate\Http\Request;
use E3Creative\ApiForceAppUpdate\Models\BlacklistedVersion;

class ForceUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $version = $request->header('App-Version');

        if ($version && $this->isVersionBlacklisted($version)) {
            return response()->json([
                'message' => config('api-force-app-update.error'),
                'code' => 4039,
            ], 403);
        }

        return $next($request);
    }

    /**
     * Determine if a version is blacklisted.
     *
     * @param $version
     * @return bool
     */
    private function isVersionBlacklisted(string $version): bool
    {
        return BlacklistedVersion::whereVersion($version)->exists();
    }
}
