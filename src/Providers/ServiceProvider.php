<?php

namespace E3Creative\ApiForceAppUpdate\Providers;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $root = __DIR__ . '/../..';

        $this->loadMigrationsFrom($root . '/migrations');

        $this->publishes([
            $root . '/config/api-force-app-update.php' => config_path('api-force-app-update.php'),
        ]);
    }
}
